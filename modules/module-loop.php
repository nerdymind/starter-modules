<?php
if ( is_singular() ) $id_to_get = ( $id_to_get ) ? $id_to_get : get_the_ID();
if ( is_tax() ) $id_to_get = get_queried_object();

if ( have_rows( 'modules', $id_to_get ) ) {
  $module_id = 0;
  $count = count( get_field( 'modules', $id_to_get ) );
  while ( have_rows( 'modules', $id_to_get ) ) {
    the_row();
    include( get_stylesheet_directory() . '/modules/module-single.php' );
  }
}
?>
