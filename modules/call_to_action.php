<?php
$calls_to_action = get_field( 'calls_to_action', 'option' );
$selection = $module['cta_selection'];
$backgroundImage = $calls_to_action[$selection]['side_image']['sizes']['large'];
$backgroundColor = $calls_to_action[$selection]['background_color'];

?>
<div class="row background-layers">
    <div class="col-12 col-md-5 background-image" style="background-image: url('<?=$calls_to_action[$selection]['side_image']['sizes']['large']?>');"></div>
    <div class="col-12 col-md-7 background-color <?=$backgroundColor?>"></div>
</div>

<div class="container" >
    <div class="row justify-content-end" id="mod-<?php echo $module_id; ?>-cta-<?php echo $selection; ?>">
    <div class="col-12 col-md-6"></div>
      <div class="col-12 col-md-6 cta-content <?=$backgroundColor?>" >
          <h3><?=$calls_to_action[$selection]['heading']?></h3>

          <div class="content cta-text">
              <?=$calls_to_action[$selection]['description_instructions']?>
          </div>

          <div class="actions">
              <?php
              foreach ( $calls_to_action[$selection]['actions'] as $action ) :
                  $action_id = uniqid();


                  if ( $action['type'] == 'call' ){
                      $link = sprintf( 'href="tel:%s"', $action['phone'] );
                  }

                  if ( $action['type'] == 'modal' ) {
                      $link = sprintf( 'href="#modal%s" data-toggle="modal"', $action_id );
                      $nm_theme = ibmc::get_instance();
                      $modals = $nm_theme->create_global_modals($action['label'], $action_id, $action['modal'] );
                  }

                  if ( $action['type'] == 'url' ){
                      $link = sprintf( 'href="%s" target="_blank"', $action['url'] );
                  }

                  if ( $action['type'] == 'link' ){
                      $link = sprintf( 'href="%s"', get_permalink( $action['link'] ) );
                  }
                  ?>
                  <a class="btn btn-outline" <?php echo $link; ?>>
                      <i class="fa <?=$action['icon']?>" aria-hidden="true"></i>
                      <?=$action['label']?>
                  </a>
              <?php endforeach; ?>
          </div>
      </div>
    </div>
</div>
