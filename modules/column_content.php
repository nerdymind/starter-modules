<div class="container" >
    <div class="row">
        <?php foreach ( $module['column_content'] as $column ) : $i++;
            $background = '';
            $color = '';
            if($column['background_image']){
                $background = 'background';
            }
            if($column['column_bg_color']){
                $color = 'color';
            }
        ?>
            <div class="col-12 col-md-6 col-lg-<?=$column['column_width']?> fw-wrapper <?=$column['column_bg_color']?> <?=$background?> <?=$color?>" style="background:<?=$column['column_bg_color_custom'] ?>; <?php if($background != '') :?> background: url('<?=$column['background_image']['sizes']['large']?>')<?php endif; ?>">
                <?=$column['column_content']?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

