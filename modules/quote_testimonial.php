<?php
//if a custom quote
if($module['custom_or_category'] == 0) :
?>
<div class="container" >
    <div class="testimonial row justify-content-end">
            <div class="col-12 col-md-6 quote">
                <div class="quote-wrapper">
                    <?php if($module['quote_photo']) : ?>
                    <span class="quote-img">
                        <img  src="<?=$module['quote_photo']['sizes']['large']?>" />
                    </span>
                    <?php else : ?>
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                    <?php endif; ?>
                    <p>"<?=$module['quote_text']?>"</p>
                    <p class="testimonial-name">- <?=$module['quote_name']?></p>
                </div>

                <?php if($module['quote_link'] != '') : ?>
                    <a class="btn btn-primary" href="<?=$module['quote_link']['url']?>">
                        <?php if($module['forward_path_link']['title'] == '')  : ?>
                            <?=$module['quote_link_icon']?> More Student Success Stories
                        <?php else : ?>
                            <?=$module['quote_link_icon']?> <?=$module['forward_path_link']['title']?>
                        <?php endif; ?>
                    </a>
                <?php endif; ?>

            </div>
    </div>
</div>

<?php
//else pull from catagory
else :
    //get catagory ID
    $cat = $module['quote_category'];

    if($cat==""){
        //if no catagory is selected show all
        $args = array(
            'post_type' => 'customer_quotes', // the name of the custom post type, or 'post' for standard blog
            'posts_per_page' => 1,
            'orderby'   => 'rand'
        );

    }else{
        $args = array(
            'post_type' => 'customer_quotes', // the name of the custom post type, or 'post' for standard blog
            'posts_per_page' => 1,
            'orderby'   => 'rand',
            'tax_query' => array(
                array(
                    'taxonomy' => 'quotes_taxonomy', // or just 'category' if pulling from Blog
                    //'field'    => 'term_taxonomy_id',
                    'terms'    => $cat
                )
            )
        );
    }

    $testimonial = new WP_Query($args);
    ?>
<div class="container" >
    <div class="testimonial row justify-content-end">
        <?php
        while( $testimonial->have_posts() ) :
            $testimonial->the_post();
            if ($module['button_text'] != ''){$buttonText = $module['button_text'];}
            else{$buttonText = 'Customer Success Stories';}
            $company_name = get_field('company_name', get_the_ID());
            ?>

            <div class="col-12 col-md-6 ">
                <div class="quote">

                    <?php if(!empty(has_post_thumbnail())) : ?>
                    <span class="quote-img">
                        <img  src="<?=the_post_thumbnail_url( 'medium')?>" />
                    </span>
                    <?php else : ?>
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                    <?php endif; ?>

                    <?=the_content();?>
                    <p class="testimonial-name">- <?=the_title();?><br><?=$company_name?></p>
                </div>
                <?php if(!empty($module['quote_link']['url'])) : ?>
                    <a class="btn btn-primary" href="<?=$module['quote_link']['url']?>">
                        <?php if($module['forward_path_link']['title'] == '')  : ?>
                            <?=$module['quote_link_icon']?> More Student Success Stories
                        <?php else : ?>
                            <?=$module['quote_link_icon']?> <?=$module['forward_path_link']['title']?>
                        <?php endif; ?>
                    </a>
                <?php endif; ?>
            </div>

            <?php
        endwhile;
        ?>
    </div>
</div>
<?php
endif;
?>