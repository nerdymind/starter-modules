<?php
   $show = get_field('announcements', 'option');
    if($show != 0 ) :
?>
<div class="container" >
    <div class="row" >
        <div class="col-12 content">
            <?=$module['announcement_content']?>
            <?php if(!empty($module['announcement_link']['url'])) : ?>
                <a class="btn btn-outline" href="<?=$module['announcement_link']['url']?>" target="<?=$module['announcement_link']['target']?>" >

                    <?php if($module['announcement_icon'] != '')  : ?>
                        <?=$module['announcement_icon']?>
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                    <?php endif; ?>

                    <?php if($module['announcement_link']['title'] == '')  : ?>
                        More Details
                    <?php else : ?>
                        <?=$module['announcement_link']['title']?>
                    <?php endif; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php endif; ?>
