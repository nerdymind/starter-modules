<div class="container" >
    <div class="row">
    <?php foreach ( $module['columns'] as $openContent ) :  ?>
        <div class="col-md-<?=$openContent['column_width']?>" >
          <?=$openContent['column_content']?>
        </div>
    <?php endforeach; ?>
    </div>
</div>