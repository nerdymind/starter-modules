<div class="container">
    <div class="row">
        <div class="<?= $module['featured_item']; ?> col-12 col-md-<?= $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' push-md-' . ( 12 - $module['item_width'] ) : ''; ?>">
            <div class="image-wrap">
                <?php if ( $module['featured_item'] == 'image' ) : ?>
                    <img src="<?= $module['image']['url']; ?>" class="img-fluid">
                <?php else : ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <?= $module['video']; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div><!-- /.col-sm-<?= $module['item_width']; ?> -->

        <div class="content space-bottom20 col-md-<?= 12 - $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' pull-md-' . $module['item_width'] : ''; ?>">
            <?= $module['content']; ?>
        </div><!-- /.col-sm-<?= 12 - $module['item_width']; ?> -->
    </div><!-- /.row -->
</div>