<?php
// get iframe HTML
$iframe = $module['video'];

// use preg_match to find iframe src
preg_match('/src="(.+?)"/', $iframe, $matches);
$src = $matches[1];

//get VIDEO_ID
preg_match('/embed(.*?)?feature/', $src, $matches );
$id = $matches[1];
$id = str_replace( str_split( '?/' ), '', $id );


?>


<script>
    //http://codepen.io/mrnathan8/pen/GqKMqZ
    //script plays on autoplays mobile devices and alows us to mute sound
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var tv,
        playerDefaults = {autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3};
    var vid = [
            {'videoId': '<?=$id?>', 'startSeconds': 0, 'endSeconds': 90, 'suggestedQuality': 'hd720'},
        ],
        randomvid = Math.floor(Math.random() * (vid.length - 1 + 1));

    function onYouTubePlayerAPIReady(){
        tv = new YT.Player('video', {events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}, playerVars: playerDefaults});
    }

    function onPlayerReady(){
        tv.loadVideoById(vid[randomvid]);
        tv.mute();
    }

    function onPlayerStateChange(e) {
        if (e.data === 1){
            //$('#tv').addClass('active');
        } else if (e.data === 0){
            tv.seekTo(vid[randomvid].startSeconds)
        }
    }

</script>



<div class="hero-bg <?php if(!empty($iframe)):?> video<?php endif; ?>" style="background-image: url('<?=$module['image']['url']?>')">


    <div class="container" >
        <div class="row justify-content-start" >
            <div class="col-12 col-md-6 content">
                <h1 class="h1" ><?=$module['main_heading']?></h1>
                <h2><?=$module['secondary_heading']?></h2>
            </div>
        </div>
    </div>


    <?php if(!empty($iframe)) : ?>
        <!-- if they select video-->
        <div class="video-wrapper">
            <div class="videobg-aspect">
                <div class="video" id="video"></div>
            </div>
        </div>
        <img class="video-thumbnail" src="http://img.youtube.com/vi/<?=$id?>/maxresdefault.jpg" />
    <?php endif; ?>

</div>

