<div class="container" >
    <div class="row">
        <?php if (!empty($module['forward_path_header'])){?>
            <h2 class="col-12 fw-header">
                <?=$module['forward_path_header'];?>
            </h2>
        <?php } ?>
    </div>

    <div class="row">
        <?php
        foreach ( $module['forward_path'] as $page_fp ) : $i++; ?>

            <div class="col-12 col-md-6 col-lg-<?=$page_fp['block_width']?> fw-wrapper">
                <a class="fw-box <?=$page_fp['forward_path_color']?>" target="<?=$page_fp['forward_path_link']['target']?>" title="<?=$page_fp['forward_path_link']['title']?>" href="<?=$page_fp['forward_path_link']['url']?>" style="background-image: url('<?=$page_fp['forward_path_image']['sizes']['large']?>')">
                    <h3  class="fw-title"><?=$page_fp['forward_path_title'];?></h3>
                    <span class="btn btn-default">
                    <?php if($page_fp['forward_path_link']['title'] == '')  : ?>
                        Learn More
                    <?php else : ?>
                        <?=$page_fp['forward_path_link']['title']?>
                    <?php endif; ?>
                </span>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
