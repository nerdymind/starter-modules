<div class="owl-carousel owl-theme">
        <?php foreach ( $module['images'] as $image ) : $i++; ?>
            <div data-featherlight-gallery data-featherlight-filter="a">
                <a class="gallery" href="<?= $image['url']; ?>" >
                    <img src="<?= $image['sizes']['large']; ?>" class="img-fluid center-block">
                    <span class="overlay">
                          <i class="fa fa-search" aria-hidden="true"></i>
                    </span>
                </a>
            </div><!-- /.col-sm-3 -->
        <?php endforeach; ?>
</div>