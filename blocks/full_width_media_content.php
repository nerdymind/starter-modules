<div class="container-fluid background">
    <div class="row">
        <div style="background: url('<?= $module['image']['url']; ?>');" class="<?= $module['featured_item']; ?> col-12 col-md-<?= $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' order-2'  : ' order-3'; ?>">

        </div>

        <div class="content col-md-<?= 12 - $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' order-3' : ' order-1'; ?>">

        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="<?= $module['featured_item']; ?> col-12 col-md-<?= $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' order-2' : ' order-3'; ?>">

        </div><!-- /.col-sm-<?= $module['item_width']; ?> -->

        <div class="content col-md-<?= 12 - $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' order-3' : ' order-1'; ?>">
            <?= $module['content']; ?>
        </div><!-- /.col-sm-<?= 12 - $module['item_width']; ?> -->
    </div><!-- /.row -->
</div>