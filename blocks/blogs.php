<?php
//get catagory ID
$cat = $block['category']->term_id;


if(is_single()){
  $ignore = get_the_ID();
}

if($cat=="" ){
  //if no catagory is selected show all
  $args = array(
    'post_type' => 'post', // the name of the custom post type, or 'post' for standard blog
    'posts_per_page' => 4,
    'post_status' => 'publish',
    'post__not_in' => array( $ignore )
  );

}elseif ($cat!=""){
  $args = array(
    'post_type' => 'post', // the name of the custom post type, or 'post' for standard blog
    'posts_per_page' => 4,
    'post_status' => 'publish',
    'orderby' => 'meta_value',
    'post__not_in' => array( $ignore ),
    'tax_query' => array(
      array(
        'taxonomy' => 'category', // or just 'category' if pulling from Blog
        'terms'    => $cat
      )
    ),

  );
}

$blogs = new WP_Query($args);
?>

<div class="container" >
  <div class="row blog-grid">

    <?php foreach ($blogs->posts as $blog) : $i;?>
      <div class="container blog-wrap mt-5 mb-5" >
        <div class="row">
          <div class="col-3 col-md-4">
            <div class="img-wrap">
              <?php if(!empty(get_the_post_thumbnail_url($blog->ID))) : ?>
                <img src="<?=get_the_post_thumbnail_url($blog->ID)?>"/>
              <?php else : ?>
                <img src="<?=get_field('header_logo', 'option')['url']?>"  alt="<?=get_bloginfo('name');?>"/>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-9 col-md-8">
            <header>
              <time class="updated" datetime="<?=get_post_time('c', true)?>"><?=get_the_date("m/d/y", $blog->ID) ?></time>
              <h3 class="entry-title"><a href="<?=get_permalink($blog->ID)?>"><?=get_the_title($blog->ID) ?></a></h3>
            </header>
            <div class="entry-summary">
              <?=get_the_excerpt($blog->ID)?>
              <a href="<?=get_permalink($blog->ID)?>">Read More...</a>
            </div>
          </div>
        </div>
      </div>

      <?php $i++; endforeach; ?>


    <div class="container " >
      <div class="row">
        <div class="col-10">
          <a class="btn btn-default" href="<?=get_category_link($cat);?>">View More</a>
        </div>
      </div>
    </div>

  </div>
</div>


