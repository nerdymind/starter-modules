<div class="container" >
<!--    <div class="row">-->
<!--        --><?php //if (!empty($block['title'])){?>
<!--            <h2 class="col-12 fw-header">-->
<!--                --><?//=$block['title'];?>
<!--            </h2>-->
<!--        --><?php //} ?>
<!--    </div>-->

    <div class="row">
        <?php
        foreach ( $block['forward_path'] as $page_fp ) : $i++; ?>

            <div class="col-12 col-md-6 col-lg-4 fw-wrapper">
                <a class="fw-box <?=$page_fp['forward_path_color']?>" target="<?=$page_fp['forward_path_link']['target']?>" title="<?=$page_fp['forward_path_link']['title']?>" href="<?=$page_fp['forward_path_link']['url']?>" >
                    <div class="img-wrap">
                      <img src="<?=$page_fp['forward_path_image']['sizes']['large']?>" />
                    </div>
                    <span class="fw-text">
                      VIEW <br/>
                    <?php if(!empty($page_fp['forward_path_link']['title']))  : ?>
                        <?=$page_fp['forward_path_link']['title']?>
                    <?php endif; ?>
                </span>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
