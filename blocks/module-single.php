<?php
global $blocks_id;
$blocks_id++;
$i = 0;

unset( $blocks_class, $background, $bg_color, $bg_color_custom, $bg_image, $blocks, $color );

$fields_to_get = [
  'background',
  'bg_color',
  'bg_color_custom',
  'bg_image',
  'mod_margin',
  'block',
  'mod_padding',
];

foreach ( $fields_to_get as $field ) {
  ${ $field } = get_sub_field( $field );

}

$block = $block[0];

if ( $i == 1 ) $blocks_class .= 'module-first';
if ( $i == $count ) $blocks_class .= ' module-last';

if ( $background != 'transparent' )
  $blocks_class .= ' bg-' . $bg_color;
?>
<section id="block-<?= $blocks_id; ?>" class="blocks <?= $block['acf_fc_layout']; ?> <?= $blocks_class; ?>" data-module="<?= $blocks['acf_fc_layout']; ?>" data-background="<?= $background; ?>" data-bg-color="<?= $bg_color; ?>" data-bg-color-custom="<?= $bg_color_custom; ?>" data-bg-image="<?= $bg_image; ?>">
  <div class="mod-cont">


    <?php

    if ( locate_template( 'views/blocks/' . $block['acf_fc_layout'] . '.php' ) ) {
      include( get_stylesheet_directory() . '/views/blocks/' . $block['acf_fc_layout'] . '.php' );
    }

    ?>
  </div><!-- /.<?= $container_class; ?> -->
</section><!-- /.modules .<?= $block['acf_fc_layout']; ?> -->

<?php if ( $background == 'color' || $background == 'image' || $background == 'pattern' || isset( $mod_margin ) || isset( $mod_padding ) ) :
  if ( $bg_color == 'primary' ) $color = $brand_primary;
  if ( $bg_color == 'secondary' ) $color = $brand_secondary;
  if ( $bg_color == 'custom' ) $color = $bg_color_custom;
?>
<style type="text/css">
#block-<?= $blocks_id; ?> {
  <?php if ( $background == 'color' && $bg_color ) : ?>
  background-color: <?= $color; ?>;
  <?php endif; ?>
  <?php if ( $background == 'image' || $background == 'pattern' ) : ?>
  background-image: url('<?= $bg_image; ?>');
  <?php endif; ?>
  <?php if ( $mod_margin ) : ?>
  margin: <?= $mod_margin; ?>;
  <?php endif; ?>
  <?php if ( $mod_padding ) : ?>
  padding: <?= $mod_padding; ?>;
  <?php endif; ?>
}
</style>
<?php endif; ?>
