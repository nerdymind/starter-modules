<div class="container" >
    <div class="row justify-content-around">
        <?php foreach ( $block['person'] as $person ) : $i++; ?>
            <div class="col-12 col-md-3 team-wrap" >
              <a href="#team-<?=$i?>" data-toggle="modal" data-target="#team-<?=$i?>">
                <div class="img-wrap">
                    <img src="<?=$person['image']['sizes']['medium']?>" />
                </div>
                <div class="content">
                    <h5><?=$person['name']?></h5>
                    <p><?=$person['title']?></p>
                </div>
              </a>
            </div>

          <!-- Modal -->
          <div class="modal fade" id="team-<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="team-<?=$i?>-label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <h3><?=$person['name']?></h3>
                  <p class="large-text"><?=$person['title']?></p>
                  <?=$person['bio_content']?>
                </div>

              </div>
            </div>
          </div>

        <?php endforeach; ?>
    </div>

</div>
