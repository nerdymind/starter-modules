
<div class="container">
        <div class="row col-12">
          <h2><?=$block['title']?></h2>
        </div>
            <div class="row accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ( $block['questions'] as $question ) : $questions++; ?>
                    <div class="col-12 col-md-6">
                        <div class="row qa-block">
                            <div class="col-2 icon-wrap">
                                <span class="plus-icon"></span>
                            </div>
                            <div class="col-10 qa-wrap">
                                <div class="q-title" role="tab" id="headingOne">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse"  href="#collapse-<?=$questions?>" role="button" aria-expanded="false" aria-controls="collapse-<?=$questions?>">
                                            <?= $question['question']; ?>
                                          <i class="fas fa-chevron-down"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="collapse"  id="collapse-<?=$questions?>" class="collapse" >
                                    <div class="card-block">
                                        <?= $question['answer']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
</div>
