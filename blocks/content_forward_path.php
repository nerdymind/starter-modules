<div class="container">
    <div class="row">
      <div class="<?= $block['featured_item']; ?> col-12 col-md-<?= $block['item_width']; ?><?= ( $block['item_placement'] == 'right' ) ? ' order-md-2' : ' order-md-1'; ?> order-2">
            <div class="image-grid-wrap">
                <?php foreach($block['images'] as $image) : ?>
                  <a href="<?=$image['url']['url']?>" target="<?=$image['url']['target']?>" class="fw-image">
                    <img src="<?=$image['image']['sizes']['medium']?>" />
                    <span>VIEW <br/> <?=$image['url']['title']?></span>
                  </a>
                <?php endforeach; ?>
            </div>
        </div><!-- /.col-sm-<?= $block['item_width']; ?> -->

        <div class="content col-md-<?= 12 - $block['item_width']; ?><?= ( $block['item_placement'] == 'right' ) ? ' order-md-1' : ' order-md-2'; ?> order-1">
            <?= $block['content']; ?>
        </div><!-- /.col-sm-<?= 12 - $block['item_width']; ?> -->
    </div><!-- /.row -->
</div>
