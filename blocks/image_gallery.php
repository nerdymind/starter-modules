<?php $i = 0; ?>
<div class="container">
    <div class="row">
        <?php foreach ( $block['images'] as $image ) : $i++; ?>
            <div class="col-6 col-md-4 " data-featherlight-gallery data-featherlight-filter="a">
                <div class="image-wrapper">
                    <a class="gallery" href="<?= $image['url']; ?>" >
                        <img src="<?= $image['sizes']['large']; ?>" class="img-fluid center-block">
                        <span class="overlay">
                          <i class="fa fa-search" aria-hidden="true"></i>
                        </span>
                    </a>
                </div>
            </div><!-- /.col-sm-3 -->
        <?php endforeach; ?>
    </div>
</div>
