<div class="container" >
  <div class="row">
    <?php foreach ( $block['icons'] as $icon ) :  ?>
      <div class="col-12 col-md-6 icon-items" >
        <div class="row">
          <div class="col-12 col-sm-4 col-lg-3 icon-img">
            <img src="<?=$icon['icon']['url']?>" class="img-fluid"/>
          </div>
          <div class="col-12 col-sm-8 col-lg-9">
            <?=$icon['icon_text']?>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
