<?php
$calls_to_action = get_field( 'calls_to_action', 'option' );
$id = $block['cta_selection'];

$n = count($calls_to_action[$id]['content_items']);
if($n == 2){
  $col = 6;
}else{
  $col = 12;
}
?>

<div class="container" >
  <div class="row" >

      <?php foreach ($calls_to_action[$id]['content_items'] as $content) :  ?>
        <div class="col-12 col-md-<?=$col?>  cta-content " >
            <?=$content['content']?>
        </div>
      <?php endforeach ?>

  </div>
</div>


  <div class="row bg-color" >
    <?php foreach ($calls_to_action[$id]['content_items'] as $content) :  ?>
      <div class="col-12 col-md-<?=$col?>  cta-content " style="background-color: <?=$content['background_color']?>;">

      </div>
    <?php endforeach ?>
  </div>

