<div class="container" >
    <div class="row">
        <?php if (!empty($module['forward_path_header'])){?>
            <h2 class="col-12 fw-header">
              <?=$module['forward_path_header'];?>
            </h2>
          <?php } ?>
    </div>

      <div class="row">
      <?php
      foreach ( $module['forward_path'] as $page_fp ) : $i++; ?>


          <div class="col-12 col-md-6 col-lg-<?=$page_fp['block_width']?> fw-wrapper">
            <a class="fw-box <?=$page_fp['forward_path_color']?>" target="<?=$page_fp['forward_path_link']['target']?>" title="<?=$page_fp['forward_path_link']['title']?>" href="<?=$page_fp['forward_path_link']['url']?>" style="border-color: <?=$page_fp['forward_path_custom_color']?>">
                <?php
                    if(!empty($page_fp['forward_path_image']['sizes']['large']) ) :
                ?>
                <span class="img-frame">
                    <img src="<?=$page_fp['forward_path_image']['sizes']['large']?>"/>
                </span>
                <?php
                 endif;
                ?>
                <h3 style="background-color:<?=$page_fp['forward_path_custom_color']?>" class="fw-title"><?=$page_fp['forward_path_title'];?></h3>
                <span class="fw-text"><?=$page_fp['forward_path_text'];?></span>
                <span class="btn btn-arrow <?=$page_fp['forward_path_color']?>">
                    <span class="btn-text">
                        <?php if($page_fp['forward_path_link']['title'] == '')  : ?>
                            Explore Programs
                        <?php else : ?>
                            <?=$page_fp['forward_path_link']['title']?>
                        <?php endif; ?>
                    </span>
                </span>
            </a>
          </div>
        <?php endforeach; ?>
    </div>

</div>