<div class="container">
    <div class="row">
        <div class="<?= $block['featured_item']; ?> col-12 col-md-<?= $block['item_width']; ?><?= ( $block['item_placement'] == 'right' ) ? ' push-md-' . ( 12 - $block['item_width'] ) : ''; ?>">
            <div class="image-wrap">
                <?php if ( $block['featured_item'] == 'image' ) : ?>
                    <img src="<?= $block['image']['url']; ?>" class="img-fluid">
                <?php else : ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <?= $block['video']; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div><!-- /.col-sm-<?= $block['item_width']; ?> -->

        <div class="content space-bottom20 col-md-<?= 12 - $block['item_width']; ?><?= ( $block['item_placement'] == 'right' ) ? ' pull-md-' . $block['item_width'] : ''; ?>">
            <?= $block['content']; ?>
        </div><!-- /.col-sm-<?= 12 - $block['item_width']; ?> -->
    </div><!-- /.row -->
</div>
