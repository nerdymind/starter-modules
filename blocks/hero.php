<?php foreach ( $block['hero'] as $hero ) :
  $v = 0;

  if (!empty($hero['video'])) :
    // get iframe HTML
    $iframe = $hero['video'];


    // use preg_match to find iframe src
    preg_match('/src="(.+?)"/', $iframe, $matches);
    $src = $matches[1];

    // add extra params to iframe src
    $params = array(
      'autoplay'    => 1,
      'background'        => 1,
      'loop'    => 1,
      'color' => 000000,
      'title'  => 0,
      'muted' => 1,
      'byline'  => 0,
      'portrait' => 0
    );

    $new_src = add_query_arg($params, $src);

    $iframe = str_replace($src, $new_src, $iframe);

    //get VIDEO_ID
    preg_match('/embed(.*?)?feature/', $src, $matches );
    $id = $matches[1];
    $id = str_replace( str_split( '?/' ), '', $id );

  endif;
  ?>


  <?php if(!empty($hero['video'])) : ?>
  <div class="modal fade bd-example-modal-lg" id="video-<?=$v?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <?=$iframe?>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php $v++; endforeach; ?>


<div class="owl-carousel">
  <?php
  foreach ( $block['hero'] as $hero ) : ?>

    <div class="hero-bg" >

      <div class="container" >
        <div class="row " >
          <div class="col-12 col-md-6 content">
            <?=$hero['content']?>
            <?php if(!empty($hero['video']))  : $v = 0; ?>
              <a href="#video-<?=$v?>" data-toggle="modal" class="btn btn-default btn-arrow btn-orange btn-lg"><?=$hero['video_text']?></a>
            <?php $v++; endif; ?>
          </div>
        </div>
      </div>

        <div class="img-wrap">
          <img class="banner-image" src="<?=$hero['image']['url']?>" alt="<?=$hero['image']['title']?>" />
        </div>

      <div class="overlay"></div>

    </div>
  <?php endforeach; ?>
</div>
